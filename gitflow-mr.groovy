import groovy.json.*
node {
    sh "printenv"
    stage('Merge or Create MR') {
        cleanWs()

        checkout scm

        // Define your GitLab repository URL
        def gitlabUrl = 'https://gitlab.com'

        // Define your GitLab project namespace and name
        def projectNamespace = 'Ayildiz1903'
        def projectName = 'gitflow-merge-request'

        // Define the source branch (the branch you want to merge)
        def sourceBranch = 'test'

        // Define the target branch (usually 'master')
        def targetBranch = 'main'

        // Define your GitLab API token (Generate one with API access in GitLab)
        def gitlabApiToken = 'glpat-_TePjsZUyrnoFLUf51s-'

        // Create a GitLab merge request (if it doesn't exist)
        def createMergeRequest = {
            def apiUrl = "${gitlabUrl}/api/v4/projects/${projectNamespace}%2F${projectName}/merge_requests"
            def postData = [source_branch: sourceBranch, target_branch: targetBranch, title: "Merge ${sourceBranch} to ${targetBranch}"]
            def response = httpRequest(
                acceptType: 'APPLICATION_JSON',
                contentType: 'APPLICATION_JSON',
                httpMode: 'POST',
                requestBody: groovy.json.JsonOutput.toJson(postData),
                customHeaders: [[name: 'PRIVATE-TOKEN', value: gitlabApiToken]],
                url: apiUrl
            )
            echo "Merge request created: ${response.status}"
        }

        // Merge the source branch into the target branch
        def mergeBranch = {
            withCredentials([usernamePassword(credentialsId: 'gitlab-Ayildiz1903', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
            
            sh "git config --global user.name 'Ayildiz1903'"
            sh "git config --global user.email 'yildizali89@hotmail.com'"
            sh "git config user.name"
            sh "git config user.email"
            sh "git credential-cache exit"
            sh "git checkout ${sourceBranch}"
            sh "git checkout ${targetBranch}"
            sh "git merge ${sourceBranch} --no-ff -m 'Merge ${sourceBranch} into ${targetBranch}'"
            sh "git remote set-url origin https://gitlab.com/Ayildiz1903/gitflow-merge-request.git"
            //sh "ssh-agent bash -c 'echo \"${PASSWORD}\" | tr -d '\\r' | ssh-add - > /dev/null && git push origin ${targetBranch}'"
            sh "git push origin ${targetBranch}"
            echo "Branch ${sourceBranch} merged into ${targetBranch}"
            //test 5
            //test2
            }
        }
        // Check if a merge request already exists
        def mergeRequestExists = {
            def apiUrl = "${gitlabUrl}/api/v4/projects/${projectNamespace}%2F${projectName}/merge_requests?source_branch=${sourceBranch}&target_branch=${targetBranch}"
            def response = httpRequest(
                acceptType: 'APPLICATION_JSON',
                customHeaders: [[name: 'PRIVATE-TOKEN', value: gitlabApiToken]],
                url: apiUrl
            )
            response.status == 200
        }

        // Run the pipeline steps
        if (mergeRequestExists()) {
            echo "Merge request already exists for ${sourceBranch} to ${targetBranch}"
        } else {
            createMergeRequest()
        }
        mergeBranch()
        //
        
    }
}